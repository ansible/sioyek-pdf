# Sioyek PDF viewer

> Sioyek is a PDF viewer designed for reading research papers and technical books.

This Ansible role installs the latest Github release of Sioyek,
and has been tested on Ubuntu 22.04, 20.04, and 18.04.6.

Note: this role cannot recover on its own from a failed build/install.
That is, if an installation or an upgrade of Sioyek fails for any reason, and
no new release of Sioyek has been published in the time since,
any subsequent run of this role will complete without error (because we
simply compare the remote version to the local version, and when finding
they are the same assume that no further action is necessary).

**In cases of a failed install/build, you need to
*manually wipe the local Sioyek repository* before running this role again.**


## Example playbook

This is an Ansible role. You will have to create an Ansible playbook yourself.
Here's a skeleton `playbook.yml` to get you started:

```
---

- hosts: all
  roles:
    - { role: sioyek-pdf, become: true, tags: sioyek-pdf }
```

To install Sioyek, run the playbook:
```
$ ansible-playbook playbook.yml --ask-become-pass --tags sioyek-pdf
```

The role contains some simple logic to avoid re-building Sioyek
if no new release was fetched from Github.

Setting up Ansible is outside the scope of this role, but you will find plenty
of guides on the web. If you are new to Ansible, enjoy the journey!


## Configuration of Sioyek (not yet implemented)

This appears like it could be very useful:

> All marks, bookmarks, highlights and portals is stored in `shared.db`.
> As the name suggests, you can copy the `shared.db` file across machines and it
> just works. Moreover, we added an option in `prefs.config` named `shared_database_path`
> which allows you to specify the path for this shared file.

From issue #29 ["Storage of highlights, bookmarks, etc."](https://github.com/ahrm/sioyek/issues/29#issuecomment-915982417).


## Sioyek depends on Qt, and Qt is a bit of a mess

I have learnt a lot about Qt in the course of upgrading it on Ubuntu 18.04.
See [issue #1 in this repo](https://codeberg.org/ansible/sioyek-pdf/issues/1),
and comments in the YAML code introduced with commits
[#8e68dfe254](https://codeberg.org/ansible/sioyek-pdf/commit/8e68dfe2549f04714dd813b88d38f26f5ec64977)
and [#9c578509af](https://codeberg.org/ansible/sioyek-pdf/commit/9c578509af73ada4b6426d7b2228aa634a0c5aab).

A few points to keep in mind:

> The environment variables set by QtChooser is for *building* applications,
> not for running applications.
> Qt itself does not require any environment variables.
> https://forum.qt.io/topic/63773/setting-qt-environment-path




## Links and notes

+ https://github.com/ahrm/sioyek
+ https://github.com/ahrm/sioyek/issues/1
+ https://news.ycombinator.com/item?id=27893303
+ https://reddit.com/r/linux/comments/osqikw/sioyek_pdf_viewer_designed_for_reading_research
