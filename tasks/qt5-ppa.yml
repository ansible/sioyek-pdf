---

# Ubuntu 18.04 is stuck on Qt 5.9
# this work-around leaves the existing Qt5 v5.9.5 as-is, but adds
# a more recent Qt5 version from PPA, and replaces the default system-wide
# Qt5 binary and library paths with the new version from PPA *only during Sioyek build*
# but otherwise avoids making permanent changes to the Qt5 configuration.

# Because Qt is so built-in to the OS, with a huge list of reverse dependencies,
# the proper solution is probably to upgrade Ubuntu itself.


# this is important to avoid having multiple different versions in the *.list file
# which is otherwise the case if we just let apt_repository do its thing
- name: "Wipe /etc/apt/sources.list.d/{{ sioyek.qt5_ppa.filename }}.list file"
  ansible.builtin.file:
    path: "/etc/apt/sources.list.d/{{ sioyek.qt5_ppa.filename }}.list"
    state: absent

- name: "Configure the PPA {{ sioyek.qt5_ppa.repo }}"
  ansible.builtin.apt_repository:
    repo: "{{ sioyek.qt5_ppa.repo }}"
    state: present
    filename: "{{ sioyek.qt5_ppa.filename }}"


# https://askubuntu.com/questions/435564/qt5-installation-and-path-configuration
# https://forum.qt.io/topic/63773/setting-qt-environment-path/12
- name: Replace default Qt version paths
  block:

    - name: Make sure directory /usr/lib/x86_64-linux-gnu/qtchooser/ exists
      ansible.builtin.file:
        path: /usr/lib/x86_64-linux-gnu/qtchooser
        state: directory

    # these edits are picked up by qtchooser which makes them
    # available system-wide during Sioyek build
    # we will revert these settings (and ldconfig too) *after* build is complete
    # because otherwise other, pre-existing programs that depend on Qt, such as
    # Okular, will fail to start
    - name: Replace default Qt version paths
      ansible.builtin.copy:
        content: |
          {{ qt5_basedir }}/bin
          {{ qt5_basedir }}/lib
        dest: /usr/lib/x86_64-linux-gnu/qtchooser/default.conf
        mode: u+rw,go+r

    # https://blog.andrewbeacock.com/2007/10/how-to-add-shared-libraries-to-linuxs.html
    - name: "Add {{ sioyek.qt5_ppa.basename }} library path to ldconfig"
      ansible.builtin.copy:
        content: |
          {{ qt5_basedir }}/lib
        dest: "/etc/ld.so.conf.d/{{ sioyek.qt5_ppa.basename }}.conf"
        mode: u+rw,go+r

    - name: Reload the list of system-wide library paths
      ansible.builtin.command: ldconfig
  # END OF BLOCK
